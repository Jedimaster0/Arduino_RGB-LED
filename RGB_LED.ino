// ==========================
// Global Variables
// ==========================

// Constants
const int ledRed = 3;
const int ledGreen = 6;
const int ledBlue = 5;
const int potentiometerPin = A0;

// Variables
int potentiometerValue;
long counter = 0;
long duration = 10000;

// You can change these
bool debugMode = false;                            // Enable serial output (debug mode)

// ==========================
// Initialization
// ==========================
void setup(){
  if (debugMode) {
    Serial.begin(9600);
  }
  
  pinMode(potentiometerPin, INPUT);
  pinMode(ledRed, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(ledBlue, OUTPUT);
}

// ==========================
// Light LED
// ==========================
void loop(){
  if (debugMode) {
    Serial.print(analogRead(potentiometerPin));
    Serial.print(", ");
    Serial.print(counter);
    Serial.println();
  }
  
  potentiometerValue = analogRead(potentiometerPin) / 4.906;
  
  if (counter <= duration){
    red(potentiometerValue);
  } else if (counter <= duration * 2){
    yellow(potentiometerValue);
  } else if (counter <= duration * 3){
    green(potentiometerValue);
  } else if (counter <= duration * 4){
    cyan(potentiometerValue);
  } else if (counter <= duration * 5){
    blue(potentiometerValue);
  } else if (counter <= duration * 6){
    violet(potentiometerValue);
  } else if (counter <= duration * 7){
    white(potentiometerValue);
  }
  counter++;
  if (counter > duration * 7){ counter = 0; }
}


// ==========================
// Functions
// ==========================
void red(int value){
  analogWrite(ledRed, value);
  analogWrite(ledGreen, 0); analogWrite(ledBlue, 0);
}

void yellow(int value){
  analogWrite(ledGreen, value); analogWrite(ledRed, value);
  analogWrite(ledBlue, 0);
}

void green(int value){
  analogWrite(ledGreen, value);
  analogWrite(ledRed, 0); analogWrite(ledBlue, 0);
}

void cyan(int value){
  analogWrite(ledGreen, value); analogWrite(ledBlue, value);
  analogWrite(ledRed, 0);
}

void blue(int value){
  analogWrite(ledBlue, value);
  analogWrite(ledRed, 0); analogWrite(ledGreen, 0);
}

void violet(int value){
  analogWrite(ledRed, value); analogWrite(ledBlue, value);
  analogWrite(ledGreen, 0);
}

void white(int value){
  analogWrite(ledGreen, value); analogWrite(ledBlue, value); analogWrite(ledRed, value);
}
